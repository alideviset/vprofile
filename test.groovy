def grv
pipeline {
    agent any
    triggers {
        cron('H/50 * * * 1-5')
    }
    // tools {
    //     maven 'apache-maven-3.0.1' 
    // }
parameters {
        string(name: 'PERSON', defaultValue: 'Mr Jenkins', description: 'Who should I say hello to?')

        text(name: 'BIOGRAPHY', defaultValue: 'birthdaty', description: 'Enter some information about the person')

        booleanParam(name: 'TOGGLE', defaultValue: true, description: 'Toggle this value')

        choice(name: 'CHOICE', choices: ['One', 'Two', 'Three'], description: 'Pick something')

        password(name: 'PASSWORD', defaultValue: 'SECRET', description: 'Enter a password')
    }
    stages {
        stage('load') {
            steps {
                script {
                    grv = load "script.groovy" 
                }
               
            }
        }
        stage('Init') {
            steps {
                script {
                    grv.initCred()
                }
                
            }
        }
        stage('Test') {
            steps {
                script {
                     grv.createProfile()
                }
               
            }
        }
        stage('Build') {
             input {
                message "Should we continue?"
                ok "Yes, we should."
                submitter "alice,bob"
                parameters {
                    string(name: 'PERSON', defaultValue: 'Mr Jenkins', description: 'Who should I say hello to?')
                }
            }
            steps {
                script {
                    grv.creatSnapshot() 
                }
                
            }
        }
        stage('Sanity check') {
            when {
                branch 'production'
                environment name: 'DEPLOY_TO', value: 'production'
            }
            steps {
                input "Does the staging environment look ok?"
            }
        }
        stage('Deploy') {
            steps {
                script {
                    grv.descriptSnapshot() 
                }
                
            }
        }
    }
}
