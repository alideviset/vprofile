def grv
properties([
  parameters([
    string( name: 'BuildConfiguration', 
            defaultValue: 'Release', 
            description: 'Configuration to build (Debug/Release/...)'),
    string( name: 'PERSON', 
            defaultValue: 'Mr Jenkins', 
            description: 'Who should I say hello to?'),
    text(   name: 'BIOGRAPHY', 
            defaultValue: 'birthdaty', 
            description: 'Enter some information about the person'),
    booleanParam(name: 'TOGGLE', 
            defaultValue: true, 
            description: 'Toggle this value'),
    choice(name: 'CHOICE', 
            choices: ['One', 'Two', 'Three'], 
            description: 'Pick something'),
    password(name: 'PASSWORD', 
            defaultValue: 'SECRET', 
            description: 'Enter a password')

  ])
])
node {
    
    stage('cloning git repo') {
        git branch: 'master', url:'https://gitlab.com/alideviset/vprofile.git'
      
    }
    stage('load') {
    
                script {
                    grv = load "script.groovy" 
                }
    
    }
  
    stage('Example') {
            grv.initCred() 
            echo 'I only execute on the master branch'
            grv.createProfile()
            echo 'I execute elsewhere'
     }
    stage('Example2') {
            sh 'exit 1'
            grv.creatSnapshot()
            echo 'Something failed, I should sound the klaxons!'
             grv.descriptSnapshot() 

    }
}
